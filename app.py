#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
from time import sleep, gmtime, strftime

import psycopg2
import requests
from requests import Timeout, ConnectionError


logging.basicConfig(filename='uz_go.log', format='%(asctime)s %(message)s', level=logging.INFO)

city_mappping = {
    '2204001': 'Kharkiv-Pas',
    '2210800': 'Zaporizhzhya 1'
}


class DataBaseConnect(object):
    msg = 'Unable to connect to the database'

    def __init__(self, dbname, user, host, password):
        try:
            self.conn = psycopg2.connect(dbname=dbname, user=user, host=host, password=password)
        except:
            logging.error(self.msg)
            raise TypeError(self.msg)

    def __del__(self):
        self.conn.close()


class UzGovClientException(Exception):
    def __init__(self, msg):
        super(UzGovClientException, self).__init__(msg)
        logging.error(msg)


class UzGovClient(object):
    uz_api = 'https://booking.uz.gov.ua/en/train_search/'
    headers = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}
    timeout = 60  # seconds
    execution_delay = 60 * 5  # seconds
    directions = ('from', 'to')

    # store queries
    stations_store_query = 'INSERT INTO station (code, name) VALUES (%(code)s, %(station)s) ON CONFLICT DO NOTHING;'
    train_store_query = 'INSERT INTO train (num, category) VALUES  (%(num)s, %(category)s) ON CONFLICT DO NOTHING;'

    def __init__(self, required_trains, from_station, to_station, date, time='00:00'):
        self.db = DataBaseConnect('uzgov', 'vitalii', 'localhost', '123kk45kk')
        self.required_trains = required_trains
        self.from_station = from_station
        self.to_station = to_station
        self.date = date  # date format yyyy-mm-dd
        self.time = time
        self.request_data = {'from': self.from_station, 'to': self.to_station,
                             'date': self.date, 'time': self.time}

    def get_uz_data(self):
        try:
            res = requests.post(self.uz_api, data=self.request_data, headers=self.headers)
        except Timeout:

            raise UzGovClientException('Timeout during fetching data from uz_go.')
        except ConnectionError:
            raise UzGovClientException('Unable to connect to uz_go.')
        try:
            return res.json()['data']['list']
        except KeyError:
            raise UzGovClientException('Unable to parse data from uz_go.')

    def store_data_to_database(self, uz_data):
        cursor = self.db.conn.cursor()
        for direction in self.directions:
            try:
                for station in uz_data:
                    cursor.execute(self.stations_store_query, station[direction])
            except:
                raise UzGovClientException('Unable to parse data from uz_go.')

        for train in uz_data:
            try:
                cursor.execute(self.train_store_query, train)
            except:
                raise UzGovClientException('Unable to parse data from uz_go.')
        cursor.close()
        self.db.conn.commit()

    def get_tickets(self, uz_data):
        res = dict()
        available_trains = list(filter(lambda train: train['num'] in self.required_trains, uz_data))
        if not available_trains:
            return res

        for train in available_trains:
            train_name = ' {} ==> {}'.format(train['from']['station'], train['to']['station'])
            res[train['num'] + train_name] = {t['title']: t['places'] for t in train['types']}
        return res

    def run(self):
        logging.info('App started')
        while True:

            uz_data = self.get_uz_data()
            self.store_data_to_database(uz_data)
            available_trains = self.get_tickets(uz_data)
            if available_trains:
                for train in available_trains:
                    if available_trains[train]:
                        print('_' * 102)
                        print('For {} tickets found'.format(train))
                        for title, places in available_trains[train].items():
                            print(title + ' ' * (100 - len(title)) + str(places))
                            print('_' * 102)
                    else:
                        msg = '{} nothing found'.format(strftime('%X', gmtime()))
                        print(msg)
                        logging.info(msg)

            sleep(self.execution_delay)


app = UzGovClient(['261О', '081О'], '2204001', '2210800', '2018-08-02')

app.run()
